# paper

Some redone textures and sprites for Paper Mario using emulation.

### Features

- Redone textures created with vectors, nothing is AI upscaled
- Most font glyphs right now
- All splash screens
- Some other textures mainly focused on UI

### Caveats

- Tested only with GlideN64 on Project 64 and BizHawk

### Installing

- Find the folder for high-res textures for your emulator (usually ./Textures)
- download project and copy "PAPER MARIO" folder into the texture directory configured in your emulator
- set GlideN64 Texture enhancement options:
  - Alternative CRC calculation: off
  - Use alpha channel fully: off

### Notes about font

- The font glyphs were created with Paper Mario Dialog by [Retriever II](http://wiki.mfgg.net/index.php?title=Retriever_II)
- For each glyph file the filename would be PAPER MARIO#__00E51099__#2#0#__373FA1D0__ _ciByRGBA.png where 00E51099 is the glyph ID and 373FA1D0 is the palette.
- Normally these textures are rendered on the fly by sprite + palette but since GlideN64 captures final rastered texture the project includes a texture for each glyph/palette combination (currently about 1800 images).

### TODO
- everything else