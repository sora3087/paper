import JSZip from 'jszip'

const zip = new JSZip()

let canvas: HTMLCanvasElement = document.createElement('canvas')
let totalCount: HTMLSpanElement = document.createElement('span')
let totalProg: HTMLProgressElement = document.createElement('progress')
let glyphProg: HTMLProgressElement = document.createElement('progress')
let ctx: CanvasRenderingContext2D = canvas.getContext('2d')

const SCALE = 4
const SIZE = 16 * SCALE
const LEFT_OFFSET = 0.5 * SCALE
const BASELINE = SIZE - (3 * SCALE)

const fileNameTemplate = (glyph: string, color: string) => `PAPER MARIO#${glyph}#2#0#${color}_ciByRGBA.png`

const paper = new FontFace('paper', 'url(./paper.f6eda5cc.ttf)');

type GlyphOptions = { x?: number, y?: number }

const files: Array<[string, Blob]> = [] 

const glyphs: Array<[string, string, GlyphOptions?]> = [
  ['a', '04A8CB12'],
  ['b', '49849F2A'],
  ['c', '507BB6A6'],
  ['d', 'BEF522A8'],
  ['e', 'ADDB9561'],
  ['f', 'E22C530F'],
  ['g', '4B3A2331', { y: -4 }],
  ['h', '974B6EFC'],
  ['i', '06C95BEC'],
  ['j', 'D25A7BE1'],
  ['k', 'EA2ED624'],
  ['l', 'C63B5BEA'],
  ['m', '237BB9EE'],
  ['n', 'DEDFA5A9'],
  ['o', '459382E0'],
  ['p', '4200996C'],
  ['q', '1FF98B6B'],
  ['r', '97686F41'],
  ['s', '8595C550'],
  ['t', '2AE2B115'],
  ['u', 'F714E27F'],
  ['v', '9349EB3F'],
  ['w', 'E70257CE'],
  ['x', '130EB902'],
  ['y', 'D015A627'],
  ['z', 'EECD9486'],
  ['A', '108B7C36'],
  ['B', '9C195DC4'],
  ['C', '60C961C8'],
  ['D', '23C0ACCA'],
  ['E', '8B3A9F90'],
  ['F', '08146E9F'],
  ['G', '8027FB66'],
  ['H', '6E5D08C8'],
  ['I', '473298D0'],
  ['J', '12800D02'],
  ['K', '17DBAA9A'],
  ['L', '00E51099'],
  ['M', '069E9CA1'],
  ['N', '2AC9ACD5'],
  ['O', '21C596D2'],
  ['P', '300A6C94'],
  ['Q', '684FFEC3'],
  ['R', '0F196A7F'],
  ['S', '9A501D18'],
  ['T', 'B01032AB'],
  ['U', '94A057CB'],
  ['V', 'FD38DC68'],
  ['W', '5BF462C2'],
  ['X', '5603998B'],
  ['Y', '6F5766A2'],
  ['Z', '0B153ECE'],
  ['1', '469BAD6F'],
  ['2', '42779BDD'],
  ['3', '53302AD5'],
  ['4', '5464FDF1'],
  ['5', '6CD350C8'],
  ['6', '06AAF858'],
  ['7', '7BEC5FBC'],
  ['8', '732A545F'],
  ['9', '58099ADD'],
  ['0', '75AA95C8'],
  ['!', '423A0E95'],
  ['@', 'F39C3504'],
  ['&', 'EF9CFDE5'],
  ['?', '3B1E98A1'],
  [';', ''],
  [',', '9A10389F'],
  ['.', '0FCC1F3A'],
  [':', '12EC0BE5'],
  ["'", 'DF0FD89F'],
  ['"', '97888A87'],
  ['/', '178EDFAF', { x: 4 }],
  ['-', 'C9E275D4'],
  // svg source
  // ['♡', '52B6A3FC'], 
  // ['☆', '00146A12'],
  // ['♪', '40987293'],
]

const colors: Array<[string, string, string]> = [
  // [code, fill, outline]
  ['0288C88B', '#ffffff', '#10399c'], // battle target
  ['1449A9E7', '#bd7bd6', '#313131'], // purple
  ['21BBFDE5', '#ad0800', '#d6d6ce'], // dialog red highlighted
  ['3510C587', '#e8b5b5', '#313131'], // pink
  ['373FA1D0', '#292921', '#d6d6ce'], // regular
  ['3E793A07', '#efde21', '#313131'], // yellow
  ['7D33BC7A', '#ffffff', '#393939'], // dialog scroll back?
  ['81B32E31', '#7b7b73', '#deded6'], // disabled
  ['8EDE8206', '#adef21', '#313131'], // green
  ['A26EEC9B', '#292921', '#84847b'], // negative?
  ['AF028E08', '#84848c', '#cecece'], // invisible?
  ['B9817287', '#f7b54a', '#313131'], // orange
  ['C3984DE7', '#d6d6d6', '#313131'], // active
  ['C428B027', '#ff736b', '#313131'], // red
  ['C4F5F086', '#638cff', '#313131'], // blue
  ['C7E9582A', '#d6d6d6', '#082942'], // battleoption
  ['CC51562C', '#d6d6d6', '#313163'], // battle items
  ['D3D1E22C', '#d6d6d6', '#315231'], // battle ability
  ['D411D82C', '#d6d6d6', '#31524a'], // battle strats
  ['E3314872', '#524221', '#f7e7c6'], // sepia
  ['E3394BE6', '#524221', '#f7e7c6'], // sepia also?
  ['EF7CCFA6', '#8ce7ef', '#313131'], // cyan
  ['FA12DDA5', '#292929', '#cec6ad'], // sepia gray?
  ['FEFA56E5', '#00e784', '#313131'], // teal
  // ['D399A42D', '', ''], //battlespirits
]

const total = glyphs.length * colors.length

const updateProgress = (glyphId: number, colorId: number) => {
  const current = glyphId * colors.length + colorId
  totalCount.innerText = `(${current+1} / ${total})`
  totalProg.value = current
  glyphProg.value = glyphId + 1
}

const render = async (str: string, fill: string, outline: string, options?: GlyphOptions): Promise<Blob> => {
  const left = LEFT_OFFSET + ((options && options.x) ? options.x : 0)
  const bottom = BASELINE + ((options && options.y) ? options.y : 0)
  ctx.clearRect(0, 0, SIZE, SIZE)
  ctx.strokeStyle = outline
  ctx.strokeText(str, left, bottom)
  ctx.fillStyle = fill
  ctx.fillText(str, left, bottom)
  return new Promise((resolve, reject) => {
    canvas.toBlob((blob) => {
      const img = new Image()
      img.src = URL.createObjectURL(blob)
      document.body.appendChild(img)
      resolve(blob)
    });
  })
}

const run = async () => {
  glyphs.forEach((glyph, glyphId) => {
    const [str, glyphCode, glyphOptions] = glyph
    if(glyphCode != '') {
      colors.forEach(async (color, colorId) => {
        const [colorCode, fill, outline] = color
        if (fill !== '' && outline !== '') {
          const data = await render(str, fill, outline, glyphOptions)
          zip.file(fileNameTemplate(glyphCode, colorCode), data)
          updateProgress(glyphId, colorId)
        }
      })
    }
  })
}

const download = () => {
  zip.generateAsync({ type: 'blob'}).then(function (blob) {
      // window.location.href = "data:application/zip;base64," + base64;
      const a = document.createElement('a');
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = 'font-glyphs.zip';
      a.click();
  }, function (err) {
    //failed to download
  });
}

document.addEventListener('DOMContentLoaded', async () => {
  canvas = document.querySelector("#view")
  canvas.width = SIZE
  canvas.height = SIZE
  canvas.style.width = "128px"
  canvas.style.height = "128px"
  canvas.style.imageRendering = "pixelated"
  ctx = canvas.getContext('2d')
  ctx.font = `${SIZE * 0.78125}px 'paper'`
  ctx.textBaseline = "alphabetic"
  ctx.lineWidth = 2 * SCALE
  totalCount = document.querySelector('#totalCount')
  totalProg = document.querySelector('#totalProg')
  totalProg.max = total
  glyphProg = document.querySelector('#glyphProg')
  glyphProg.max = colors.length
  await paper.load().then(run)
  document.querySelector("#download")?.addEventListener('click', download)
})
